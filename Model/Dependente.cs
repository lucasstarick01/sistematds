﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Model
{
    public class Dependente
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public DateTime DataNasc { get; set; }
        public Socio Socio { get; set; }

        public Dependente()
        {
            Socio = new Socio();
        }

        public DataTable Select()
        {
            DataTable dependentes = new DataTable();

            try
            {
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = Properties.Settings.Default.ConnectionString;

                    connection.Open();

                    SqlCommand command = new SqlCommand();

                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT id,nome,dataNasc FROM dependente " +
                                          "WHERE idSocio = @idSocio;";

                    command.Parameters.Add("idSocio", SqlDbType.Int).Value = Socio.Id;

                    SqlDataReader query = command.ExecuteReader();

                    if(query.HasRows)
                        dependentes.Load(query);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format(
                        "Erro no Sistema DependenteModel -> Select: {0}",
                        ex.Message
                    ),
                    "AVISO",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
            }

            return dependentes;
        }

        public bool Insert()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection())
                {
                    //Definir string de conexão
                    connection.ConnectionString = Properties.Settings.Default.ConnectionString;
                    //Abrir conexão
                    connection.Open();
                    //Definir qual comando será executado
                    SqlCommand command = new SqlCommand();

                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = "INSERT INTO dependente ([nome],[dataNasc],[idSocio]) " +
                                          "VALUES (@nome, @dataNasc, @idSocio) " +
                                          "SELECT SCOPE_IDENTITY(); ";
                    //Setar parâmetros se eles existirem
                    command.Parameters.Add("nome", SqlDbType.VarChar).Value = Nome;
                    command.Parameters.Add("dataNasc", SqlDbType.Date).Value = DataNasc;
                    command.Parameters.Add("idSocio", SqlDbType.Int).Value = Socio.Id;
                    //Armazenar retorno da execução do comando (Se existir)
                    Id = int.Parse(command.ExecuteScalar().ToString());

                    if (Id != 0) //Verificar se o comando foi executado com sucesso
                        return true;

                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format(
                        "Erro no sistema DependenteSocio -> Insert: {0}",
                        ex.Message
                    ),    
                    "AVISO",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );

                return false;
            }
        }

        public bool Update()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = Properties.Settings.Default.ConnectionString;

                    connection.Open();

                    SqlCommand command = new SqlCommand();

                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = "UPDATE dependente " +
                                          "SET nome = @nome, dataNasc = @dataNasc " +
                                          "WHERE id = @id;";

                    command.Parameters.Add("id", SqlDbType.Int).Value = Id;
                    command.Parameters.Add("nome", SqlDbType.VarChar).Value = Nome;
                    command.Parameters.Add("dataNasc", SqlDbType.Date).Value = DataNasc;

                    if (command.ExecuteNonQuery() == 1)
                        return true;

                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format(
                        "Erro no sistema DependenteModel -> Update: {0}",
                        ex.Message
                    ),
                    "AVISO",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );

                return false;
            }
        }

        public bool Delete()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection())
                {
                    //Criar conexão
                    connection.ConnectionString = Properties.Settings.Default.ConnectionString;

                    //Abre a conexão
                    connection.Open();
                    
                    //Defino qual comando será executado
                    SqlCommand command = new SqlCommand();

                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = "DELETE dependente " +
                                          "WHERE id = @id;";

                    command.Parameters.Add("id", SqlDbType.Int).Value = Id;

                    //Executar o comando e verificar se tudo deu certo
                    if (command.ExecuteNonQuery() == 1)
                        return true;

                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format(
                        "Erro no sistema DependenteModel -> Delete: {0}",
                        ex.Message
                    ),
                    "AVISO",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );

                return false;
            }
        }
    }
}
