﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace View
{
    public partial class TelaDependente : Form
    {
        public Form Tela { get; set; }
        public Socio Socio { get; set; }

        public TelaDependente()
        {
            InitializeComponent();
        }

        private void TelaDependente_Load(object sender, EventArgs e)
        {
            this.listarDependentes();

            txtNome.Focus();

            this.mode(true); //insert
        }

        private void TelaDependente_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Quando a tela de dependente estiver fechando(Closing)
            //desejo abrir a tela de socio
            Tela.Show();
        }

        private void dataGridDependetes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtId.Text = dataGridDependetes.CurrentRow.Cells[0].Value.ToString();
            txtNome.Text = dataGridDependetes.CurrentRow.Cells[1].Value.ToString();
            txtDataNasc.Text = dataGridDependetes.CurrentRow.Cells[2].Value.ToString();

            this.mode(false); //update delete
        }
    
        private void listarDependentes()
        {
            /*O método SELECT da model Dependente necessita
             do id do socio para selecionar os dependentes*/

            /*Passando o socio*/
            /*this.Socio*/
            dataGridDependetes.DataSource = new Dependente
            {
                Socio = this.Socio
            }.Select();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            //ErrorProvider
            if (!txtNome.Text.Equals(""))
            {
                if (!txtDataNasc.TextNoFormatting().Equals(""))
                {
                    Dependente dependente = new Dependente();

                    dependente.Nome = txtNome.Text;
                    dependente.DataNasc = Convert.ToDateTime(txtDataNasc.Text);
                    dependente.Socio = this.Socio;

                    if (dependente.Insert())
                    {
                        MessageBox.Show(
                            string.Format(
                                "Cadastrado realizado com sucesso"
                             ),
                             "AVISO",
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Information
                        );

                        this.listarDependentes();

                        this.clearFields();
                    }
                    else
                    {
                        MessageBox.Show(
                            string.Format(
                                "Falha ao cadastrar o dependente"
                             ),
                             "AVISO",
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Hand
                        );
                    }
                }
                else
                {
                    MessageBox.Show(
                        "Favor digitar o campo Data de Nascimento",
                        "AVISO",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Hand
                    );

                    txtDataNasc.Focus();
                }
            }
            else
            {
                MessageBox.Show(
                    "Favor digitar o campo nome",
                    "AVISO",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Hand
                );

                txtNome.Focus();
            }
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            if (!txtId.Text.Equals(""))
            {
                if (!txtNome.Text.Equals(""))
                {
                    if (!txtDataNasc.Text.Equals(""))
                    {
                        Dependente dependente = new Dependente();

                        dependente.Id = int.Parse(txtId.Text);
                        dependente.Nome = txtNome.Text;
                        dependente.DataNasc = Convert.ToDateTime(txtDataNasc.Text);

                        if (dependente.Update())
                        {
                            MessageBox.Show(
                                "Sócio atualizado com sucesso",
                                "AVISO",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information
                            );

                            this.listarDependentes();

                            this.clearFields();

                            this.mode(true); //insert
                        }
                        else
                        {
                            MessageBox.Show(
                                "Falha ao atualizar o dependente",
                                "AVISO",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Hand
                            );
                        }
                    }
                    else
                    {
                        MessageBox.Show(
                            string.Format(
                                "Por favor preencha o campo Data de Nascimento!!"
                            ),
                            "AVISO",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Hand
                        );

                        txtDataNasc.Focus();
                    }
                }
                else
                {
                    MessageBox.Show(
                        string.Format(
                            "Por favor preencha o campo nome!!"
                        ),
                        "AVISO",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Hand
                    );

                    txtNome.Focus();
                }
            }
            else
            {
                MessageBox.Show(
                    string.Format(
                        "Por favor selecione um registro!!"
                    ),
                    "AVISO",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Hand
                );
            }
        }

        private void btnDeletar_Click(object sender, EventArgs e)
        {
            if (!txtId.Text.Equals(""))
            {
                if (new Dependente { Id = int.Parse(txtId.Text) }.Delete())
                {
                    MessageBox.Show(
                        "Sócio atualizado com sucesso",
                        "AVISO",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information
                    );

                    this.listarDependentes();

                    this.clearFields();

                    this.mode(true); //insert
                }
                else
                {
                    MessageBox.Show(
                        "Falha ao remover o dependente",
                        "AVISO",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Hand
                    );
                }
            }
            else
            {
                MessageBox.Show(
                    string.Format(
                        "Por favor selecione um registro!!"
                    ),
                    "AVISO",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Hand
                );
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.clearFields();

            this.mode(true); //insert
        }
        
        private void clearFields()
        {
            txtId.Clear();
            txtNome.Clear();
            txtDataNasc.Clear();

            txtNome.Focus();
        }

        private void mode(bool mode)
        {
            //Mode == true => Insert
            //Mode == false => Update/Delete
            if (mode)
            {
                btnCadastrar.Enabled = true;
                btnAtualizar.Enabled = false;
                btnDeletar.Enabled = false;
            }
            else
            {
                btnCadastrar.Enabled = false;
                btnAtualizar.Enabled = true;
                btnDeletar.Enabled = true;
            }
        }

        }
}
