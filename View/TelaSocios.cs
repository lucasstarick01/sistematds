﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace View
{
    public partial class TelaSocios : Form
    {
        //Atributo -> Propriedades
        public Form Tela { get; set; }

        public TelaSocios()
        {
            InitializeComponent();
        }

        private void TelaSocios_Load(object sender, EventArgs e)
        {
            /*Socio socio = new Socio();
            DataTable listaSocios = socio.Select();
            dataGridSocios.DataSource = listaSocios;*/

            this.listarSocios();

            txtNome.Focus();

            this.mode(true); // insert
        }

        private void TelaSocios_FormClosing(object sender, FormClosingEventArgs e)
        {
            Tela.Show();
        }
        
        private void dataGridSocios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtId.Text = dataGridSocios.CurrentRow.Cells[0].Value.ToString();
            txtNome.Text = dataGridSocios.CurrentRow.Cells[1].Value.ToString();
            txtCpf.Text = dataGridSocios.CurrentRow.Cells[2].Value.ToString();

            this.mode(false); //update delete
        }

        private void listarSocios()
        {
            dataGridSocios.DataSource = new Socio().Select();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            //ErrorProvider
            if (!txtNome.Text.Equals(""))
            {
                if (!txtCpf.Text.Equals(""))
                {
                    Socio socio = new Socio();

                    socio.Nome = txtNome.Text;
                    socio.Cpf = txtCpf.Text;

                    if (socio.Insert())
                    {
                        MessageBox.Show(
                            "Registro inserido com sucesso",
                            "AVISO",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information
                        );

                        this.listarSocios();

                        this.clearFields();
                    }
                    else
                    {
                        MessageBox.Show(
                            "Falha ao inserir o registro",
                            "AVISO",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information
                        );
                    }
                }
                else
                {
                    MessageBox.Show(
                        "Favor digitar o campo cpf",
                        "AVISO",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Hand
                    );

                    txtCpf.Focus();
                }
            }
            else
            {
                MessageBox.Show(
                    "Favor digitar o campo nome",
                    "AVISO",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Hand
                );

                txtNome.Focus();
            }
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            if (!txtId.Text.Equals(""))
            {
                if (!txtNome.Text.Equals(""))
                {
                    if (!txtCpf.Text.Equals(""))
                    {
                        Socio socio = new Socio();

                        socio.Id = int.Parse(txtId.Text);
                        socio.Nome = txtNome.Text;
                        socio.Cpf = txtCpf.Text;

                        if (socio.Update())
                        {
                            MessageBox.Show(
                                "Sócio atualizado com sucesso",
                                "AVISO",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information
                            );

                            this.listarSocios();

                            this.clearFields();

                            this.mode(true); //insert
                        }
                        else
                        {
                            MessageBox.Show(
                                "Falha ao atualizar o sócio",
                                "AVISO",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Hand
                            );
                        }
                    }
                    else
                    {
                        MessageBox.Show(
                            string.Format(
                                "Por favor preencha o campo cpf!!"
                            ),
                            "AVISO",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Hand
                        );

                        txtCpf.Focus();
                    }
                }
                else
                {
                    MessageBox.Show(
                        string.Format(
                            "Por favor preencha o campo nome!!"
                        ),
                        "AVISO",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Hand
                    );

                    txtNome.Focus();
                }
            }
            else
            {
                MessageBox.Show(
                    string.Format(
                        "Por favor selecione um registro!!"
                    ),
                    "AVISO",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Hand
                );
            }
        }

        private void btnDeletar_Click(object sender, EventArgs e)
        {
            if (!txtId.Text.Equals(""))
            {
                if (new Socio { Id = int.Parse(txtId.Text) }.Delete())
                {
                    MessageBox.Show(
                        "Sócio deletado com sucesso",
                        "AVISO",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information
                    );

                    this.listarSocios();

                    this.clearFields();

                    this.mode(true); //insert
                }
                else
                {
                    MessageBox.Show(
                        "Falha ao remover o sócio",
                        "AVISO",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Hand
                    );
                }
            }
            else
            {
                MessageBox.Show(
                    string.Format(
                        "Por favor selecione um registro!!"
                    ),
                    "AVISO",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Hand
                );
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.clearFields();

            this.mode(true); //insert
        }

        private void btnDependentes_Click(object sender, EventArgs e)
        {
            //Chamar a telaDependente (Show) passando o objeto Socio
            //que ela precisa pra selecionar os dependentes

            //Passando o objeto socio com o seu id
            /*
             new Socio {
                    Id = int.Parse(dataGridSocios.CurrentRow.Cells[0].Value.ToString())
                }
             */
            new TelaDependente
            {
                Tela = this,
                Socio = new Socio
                {
                    Id = int.Parse(dataGridSocios.CurrentRow.Cells[0].Value.ToString())
                }
            }.Show();

            this.Hide();
        }

        private void clearFields()
        {
            txtId.Clear();
            txtNome.Clear();
            txtCpf.Clear();

            txtNome.Focus();
        }

        private void mode(bool mode)
        {
            //Mode == true => Insert
            //Mode == false => Update/Delete
            if (mode)
            {
                btnCadastrar.Enabled = true;
                btnAtualizar.Enabled = false;
                btnDeletar.Enabled = false;
            }
            else
            {
                btnCadastrar.Enabled = false;
                btnAtualizar.Enabled = true;
                btnDeletar.Enabled = true;
            }
        }
    }
}
