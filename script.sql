CREATE DATABASE bdClubeTDS;

USE bdClubeTDS;

CREATE TABLE socio(
	id     INT         NOT NULL PRIMARY KEY IDENTITY(1,1),
	nome   VARCHAR(50) NOT NULL,
	cpf    VARCHAR(14) NOT NULL
);

CREATE TABLE dependente(
	id         INT         NOT NULL PRIMARY KEY IDENTITY(1,1),
	nome       VARCHAR(50) NOT NULL,
	dataNasc   DATE        NOT NULL,
	idSocio    INT         NOT NULL,
	FOREIGN KEY (idSocio) REFERENCES socio(id)
);

INSERT INTO socio (nome,cpf) VALUES ('Lucas Starick','12345678912');